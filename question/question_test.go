package question

import (
	"testing"
)

func TestQeustion1(t *testing.T) {
	input := []int64{1, 2, 54, 5, 3, 100, 0, -23}
	if GetDValue1(input) != 123{
		t.Fatalf("GetDValue1 expected=%d, actual=%d", 123, GetDValue1(input))
	}
	if GetDValue2(input) != 123{
		t.Fatalf("GetDValue2 expected=%d, actual=%d", 123, GetDValue1(input))
	}

	t.Log("done")
}

func TestQeustion2(t *testing.T) {
	poke := [54]string{"A~0","2~0","3~0","4~0","5~0","6~0","7~0","8~0","9~0","10~0","J~0","Q~0","K~0",
		"A~1","2~1","3~1","4~1","5~1","6~1","7~1","8~1","9~1","10~1","J~1","Q~1","K~1",
		"A~2","~2~2","3~2","4~2","5~2","6~2","7~2","8~2","9~2","10~2","J~2","Q~2","K~2",
		"A~3","2~3","~3~3","4~3","5~3","6~3","7~3","8~3","9~3","10~3","J~3","Q~3","K~3",
		"queen", "king"}
	Shuffle(&poke, 1)
	pokeBak := [54]string{"A~0","2~0","3~0","4~0","5~0","6~0","7~0","8~0","9~0","10~0","J~0","Q~0","K~0",
		"A~1","2~1","3~1","4~1","5~1","6~1","7~1","8~1","9~1","10~1","J~1","Q~1","K~1",
		"A~2","~2~2","3~2","4~2","5~2","6~2","7~2","8~2","9~2","10~2","J~2","Q~2","K~2",
		"A~3","2~3","~3~3","4~3","5~3","6~3","7~3","8~3","9~3","10~3","J~3","Q~3","K~3",
		"queen", "king"}
	if poke == pokeBak{
		t.Fatalf("Shuffle error")
	}
	t.Log("done")
}

func TestQeustion3(t *testing.T) {
	c := Cache{}
	c.Init()
	c.Put("hello")
	c.Put("world")
	if c.Get("hi") != nil{
		t.Fatalf("cache get error")
	}
	if c.Get("hello") == nil{
		t.Fatalf("cache get error")
	}
	//...其它暂时忽略

	t.Log("done")
}

func TestQeustion4(t *testing.T) {
	v := JosephQuestion(3, 2)
	if v != 2{
		t.Fatalf("JosephQuestion expected=%d, actual=%d", 2, JosephQuestion(3, 2))
	}
	t.Log("done")
}