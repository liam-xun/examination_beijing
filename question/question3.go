package question

import (
	"errors"
	"time"
)

//我把过期时间的处理
type Cache struct {
	members map[string]*Element
}

type Element struct {
	Data      string
	StoreTime int64
}

const ExpireTime = 120000

func (C *Cache) Init()  {
	C.members = make(map[string]*Element)
}

func (C *Cache) Get(_s string) *Element {
	if value, ok := C.members[_s]; ok {
		if value.StoreTime+ExpireTime < time.Now().Unix() {
			return nil
		}
		return value
	}
	return nil
}

func (C *Cache) Put(_s string) error {
	if value, ok := C.members[_s]; ok {
		if value.StoreTime+ExpireTime < time.Now().Unix() {
			C.members[_s] = &Element{
				Data:      _s,
				StoreTime: time.Now().Unix(),
			}
		} else {
			return errors.New("existed")
		}
	}

	C.members[_s] = &Element{
		Data:      _s,
		StoreTime: time.Now().Unix(),
	}

	return nil
}

func (C *Cache) Erase(_s string) error {
	if _, ok := C.members[_s]; ok {
		delete(C.members, _s)
		return nil
	}
	return errors.New("no existed")
}

func (C *Cache) Replace(_dest string, _s string) error {
	if _, ok := C.members[_dest]; ok {
		C.members[_dest].Data = _s
		C.members[_dest].StoreTime = time.Now().Unix()
		return nil
	}
	return errors.New("no existed")
}
