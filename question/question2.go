package question

import (
	"math/rand"
	"time"
)

//洗牌可以看做任一位置的牌和另一个位置的牌两两交换
func Shuffle(_dest *[54]string, _count int) {
	rand.Seed(time.Now().UnixNano())
	var stepLength, allNumber = len(_dest), len(_dest)
	for i := 0; i < _count; i++ {
		pokeIndex := rand.Intn(allNumber)
		l := rand.Intn(stepLength)
		if pokeIndex+l >= allNumber {
			t := _dest[pokeIndex]
			_dest[pokeIndex] = _dest[pokeIndex+l-allNumber]
			_dest[pokeIndex+l-allNumber] = t
		} else {
			t := _dest[pokeIndex]
			_dest[pokeIndex] = _dest[pokeIndex+l]
			_dest[pokeIndex+l] = t
		}
	}
}
