package question

//第一个思路
func GetDValue1(_arr []int64) int64 {
	if len(_arr) <= 0 {
		panic("input error")
	}
	min, max := _arr[0], _arr[0]
	for i := 1; i < len(_arr); i++ {
		if _arr[i] < min {
			min = _arr[i]
		}
		if _arr[i] > max {
			max = _arr[i]
		}
	}
	return max - min
}

//第二个思路,对输入做排序
func quickSort(q []int64, l, r int) {
	if l >= r { // 终止条件
		return
	}
	x := q[(l+r)>>1] // 确定分界点
	i, j := l-1, r+1 // 两个指针，因为do while要先自增/自减
	for i < j {      // 每次迭代
		for { // do while 语法
			i++ // 交换后指针要移动，避免没必要的交换
			if q[i] >= x {
				break
			}
		}
		for {
			j--
			if q[j] <= x {
				break
			}
		}
		if i < j { // swap 两个元素
			q[i], q[j] = q[j], q[i]
		}
	}
	quickSort(q, l, j) // 递归处理左右两段
	quickSort(q, j+1, r)
}

func GetDValue2(_arr []int64) int64 {
	if len(_arr) <= 0 {
		panic("input error")
	}

	quickSort(_arr, 0, len(_arr)-1)

	return _arr[len(_arr)-1] - _arr[0]
}
