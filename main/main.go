package main

import (
	"examination/question"
	"fmt"
)

func main()  {
	input := []int64{1, 2, 54, 5, 3, 100, 0, -23}
	fmt.Println(question.GetDValue1(input))
	fmt.Println(question.GetDValue2(input))

	poke := [54]string{"A~0","2~0","3~0","4~0","5~0","6~0","7~0","8~0","9~0","10~0","J~0","Q~0","K~0",
		"A~1","2~1","3~1","4~1","5~1","6~1","7~1","8~1","9~1","10~1","J~1","Q~1","K~1",
		"A~2","~2~2","3~2","4~2","5~2","6~2","7~2","8~2","9~2","10~2","J~2","Q~2","K~2",
		"A~3","2~3","~3~3","4~3","5~3","6~3","7~3","8~3","9~3","10~3","J~3","Q~3","K~3",
	"queen", "king"}
	question.Shuffle(&poke, 100)
	fmt.Println(poke)

	c := question.Cache{}
	c.Init()
	c.Put("hello")
	c.Put("world")
	fmt.Println(c.Get("hi"))
	fmt.Println(c.Get("hello"))

	fmt.Println(question.JosephQuestion(1000, 7))
}
